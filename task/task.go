package task

import (
	"github.com/mileusna/crontab"
)

func StartTasks() {
	StartCrontab()
}

func StartCrontab()  {
	tab := crontab.New()
	tab.MustAddJob("*/1 * * * *", func() {
		//log.Warn("hello world")
	})
}
