package model

import "gorm.io/gorm"

type Sex uint8

type User struct {
	gorm.Model
	Username string
	Password string
	Salt string
	Sex Sex
}
