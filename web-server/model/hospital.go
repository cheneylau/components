package model

import (
	"gorm.io/gorm"
	"time"
)

type ResultListDoctor struct {
	Code   int `json:"code"`
	Result []Doctor `json:"result"`
	Error interface{} `json:"error"`
}

type ResultListKeShi struct {
	Code   int `json:"code"`
	Result []KeShi `json:"result"`
	Error interface{} `json:"error"`
}

type ResultDoctorScheduling  struct {
	Code   int `json:"code"`
	Result DoctorScheduling `json:"result"`
	Error interface{} `json:"error"`
}

type Doctor struct {
	ID                   int    `json:"Id"`
	AppID                string `json:"AppId"`
	DoctorWorkNum        string `json:"DoctorWorkNum"`
	DeptCode             string `json:"DeptCode"`
	DeptName             string `json:"DeptName"`
	DoctorName           string `json:"DoctorName"`
	Title                string `json:"Title"`
	DoctorLevel          string `json:"DoctorLevel"`
	DoctorLevelName      string `json:"DoctorLevelName"`
	Profession           string `json:"Profession"`
	ImageURL             string `json:"ImageUrl"`
	Description          string `json:"Description"`
	Sort                 int    `json:"Sort"`
	Status               int    `json:"Status"`
	DoctorMappDeptStatus int    `json:"DoctorMappDeptStatus"`
	AppointmentType      string `json:"AppointmentType"`
	RegisterType         string `json:"RegisterType"`
	AppointmentDate      string `json:"AppointmentDate"`
	RegisterDate         string `json:"RegisterDate"`
	HasSchedule          bool   `json:"HasSchedule"`
}

type Scheduling struct {
	PlanID          string      `json:"PlanId"`
	Date            string      `json:"Date"`
	StartTime       string      `json:"StartTime"`
	EndTime         string      `json:"EndTime"`
	CanAppointment  int         `json:"CanAppointment"`
	Appointment     int         `json:"Appointment"`
	CanExcess       int         `json:"CanExcess"`
	RegTypeCode     string      `json:"RegTypeCode"`
	RegTypeName     interface{} `json:"RegTypeName"`
	DeptCode        string      `json:"DeptCode"`
	DoctorWorkNum   string      `json:"DoctorWorkNum"`
	Price           float64     `json:"Price"`
	NeedPay         int         `json:"NeedPay"`
	Week            int         `json:"Week"`
	TimeFlag        int         `json:"TimeFlag"`
	HosCode         string      `json:"HosCode"`
	OrderType       interface{} `json:"OrderType"`
	DeptName        string      `json:"DeptName"`
	ShowDeptCode    string      `json:"ShowDeptCode"`
	ShowDeptName    string      `json:"ShowDeptName"`
	DisplayName     interface{} `json:"DisplayName"`
	DoctorName      interface{} `json:"DoctorName"`
	AccessWay       interface{} `json:"AccessWay"`
	HosName         interface{} `json:"HosName"`
	DataID          interface{} `json:"DataId"`
	OrderXh         interface{} `json:"OrderXh"`
	AppointmentTime interface{} `json:"AppointmentTime"`
	Location        interface{} `json:"Location"`
	PbDocWorkNum    interface{} `json:"PbDocWorkNum"`
	PbDeptCode      interface{} `json:"PbDeptCode"`
}

type AppointmentScheduling struct {
	Groups      string `json:"Groups"`
	Schedulings []Scheduling `json:"Schedulings"`
}

type DoctorScheduling struct {
	Doctor Doctor `json:"Doctor"`
	OutScheduling         interface{} `json:"OutScheduling"`
	AppointmentScheduling []AppointmentScheduling `json:"AppointmentScheduling"`
}

type OkDoctor struct {
	Date string
	DateTime time.Time
	Doctor Doctor
}

type KeShi struct {
	AppID            string      `json:"AppId"`
	ParentCode       string      `json:"ParentCode"`
	ParentName       string      `json:"ParentName"`
	HisDeptCode      string      `json:"HisDeptCode"`
	HisDeptName      string      `json:"HisDeptName"`
	DisplayName      interface{} `json:"DisplayName"`
	HisDeptGroupType interface{} `json:"HisDeptGroupType"`
	Description      string      `json:"Description"`
	RegisterType     interface{} `json:"RegisterType"`
	AppointmentType  interface{} `json:"AppointmentType"`
	Status           int         `json:"Status"`
	ImageURL         interface{} `json:"ImageUrl"`
	HasSchedule      bool        `json:"HasSchedule"`
	Prompt           interface{} `json:"Prompt"`
	IsClinical       interface{} `json:"IsClinical"`
	IsVisit          interface{} `json:"IsVisit"`
	IsCharacteristic interface{} `json:"IsCharacteristic"`
	IsRegister       interface{} `json:"IsRegister"`
	IsMedical        interface{} `json:"IsMedical"`
	IsWard           interface{} `json:"IsWard"`
	IsSurgery        interface{} `json:"IsSurgery"`
	IsTreatMent      interface{} `json:"IsTreatMent"`
	IsExecutive      interface{} `json:"IsExecutive"`
	Sort             int         `json:"Sort"`
	TypeSort         int         `json:"TypeSort"`
	ID               int         `json:"Id"`
	IsImageURL       bool        `json:"IsImageUrl"`
	IsDescription    bool        `json:"IsDescription"`
	Num              int         `json:"Num"`
}

type ObserveInfo struct {
	gorm.Model
	WhoYuYue string
	KeShiName string
	DoctorName string
	HopeDate string //如果为空，则表示有就消息提示，填写日期表示为指定日期提示
}

