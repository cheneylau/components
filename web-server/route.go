package web_server

import (
	"gitee.com/cheneylau/components/com"
	"gitee.com/cheneylau/components/web-server/controller"
	"gitee.com/cheneylau/components/web-server/controller/user"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"path"
)

func ConfigRoute(r *gin.Engine)  {
	r.Use(gin.Logger())
	r.Use(com.Cors())
	r.Use(gin.Recovery())
	r.StaticFS("/static", http.Dir(path.Dir(os.Args[0])+"/static"))
	r.LoadHTMLGlob(path.Dir(os.Args[0])+"/static/templates/**/*")

	r.GET("/", func(context *gin.Context) {
		context.Redirect(302, "/static/hospital")
	})
	authorized := r.Group("/hospital")
	authorized.POST("/list", controller.HospitalList)
	authorized.POST("/add", controller.HospitalAdd)
	authorized.POST("/delete", controller.HospitalDelete)

	apiGroup := r.Group("api")
	v1 := apiGroup.Group("v1")
	userGroup := v1.Group("users")
	userGroup.GET("/", user.User)
	userGroup.POST("/", user.User)
	userGroup.Any(":id", user.User)
}
