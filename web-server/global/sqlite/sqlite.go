package sqlite

import (
	"gitee.com/cheneylau/components/web-server/model"
	_ "github.com/mattn/go-sqlite3"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
	"path"
)

var DB *gorm.DB

func init() {
	initSQLite()
}

func initSQLite()  {
	//https://gorm.io/docs/
	dsn := path.Join(path.Dir(os.Args[0]), "/sqlite.db")
	db, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect SQLite database")
	}
	// Migrate the schema
	db.AutoMigrate(&model.ObserveInfo{})

	DB = db
}
