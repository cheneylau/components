package global

import (
	"gitee.com/cheneylau/components/web-server/model"
	"github.com/astaxie/beego/cache"
)

var MemoryCache cache.Cache
var ObserveList []model.ObserveInfo

func init() {
	MemoryCache, _ = cache.NewCache("memory", `{"interval":60}`)
}
