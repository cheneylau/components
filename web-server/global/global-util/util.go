package global_util

import (
	"github.com/astaxie/beego/config"
	"log"
	"os"
	"path/filepath"
)

var appCnf config.Configer

func AppConfig() (config.Configer, error) {
	if appCnf == nil {
		cnf, err := config.NewConfig("ini", "conf/app.conf")
		if err != nil {
			return nil, err
		}
		appCnf = cnf
	}
	return appCnf, nil
}

func AppRunMode() string {
	cnf, err := AppConfig()
	var runmode string
	if err != nil {
		runmode = "dev"
	} else {
		runmode = cnf.String("runmode")
	}
	return runmode
}

func AppConfigWithEnv(key string) string {
	runmode := appCnf.String("runmode")
	return appCnf.String(runmode+"::"+key)
}

func AppConfigLevel1(key string) string {
	AppConfig()
	return appCnf.String(key)
}

func CheckErr(err error, msg string) {
	if err != nil {
		log.Println(msg, err)
	}
}

func AppPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	return dir
}