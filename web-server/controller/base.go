package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

type Res struct {
	Code int `json:"code"`
	Message string `json:"message"`
}

type ResMap struct {
	Res
	Data map[string]interface{} `json:"data"`
}

type ResList struct {
	Res
	Data []interface{} `json:"data"`
}

func EchoJsonMap(c *gin.Context, res *ResMap, err *error)  {
	if *err != nil {
		res.Code = 400
		res.Message = fmt.Sprintf("%v", *err)
		c.JSON(200, res)
	} else {
		res.Code = 200
		c.JSON(200, res)
	}
}

func EchoJsonList(c *gin.Context, res *ResList, err *error)  {
	if *err != nil {
		res.Code = 400
		res.Message = fmt.Sprintf("%v", *err)
		c.JSON(200, res)
	} else {
		res.Code = 200
		c.JSON(200, res)
	}
}

func BaseMapData(c *gin.Context) (*ResMap, *error) {
	data := make(map[string]interface{})
	res := ResMap{
		Res:Res {
			Code:    200,
			Message: "Success!",
		},
		Data: data,
	}

	var err error
	return &res, &err
}

func BaseListData(c *gin.Context) (*ResList, *error) {
	data := make([]interface{}, 0)
	res := ResList{
		Res:Res {
			Code:    200,
			Message: "Success!",
		},
		Data: data,
	}

	var err error
	return &res, &err
}