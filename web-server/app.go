package web_server

import (
	global_util "gitee.com/cheneylau/components/web-server/global/global-util"
	"github.com/gin-gonic/gin"
)

func StartHttpServer() {
	r := gin.New()
	ConfigRoute(r)
	// 监听并在 0.0.0.0:8080 上启动服务
	port := global_util.AppConfigLevel1("httpport")
	r.Run(":"+port)
}
