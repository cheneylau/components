module gitee.com/cheneylau/components

go 1.16

require (
	github.com/astaxie/beego v1.12.1
	github.com/cheneylew/gotools v0.2.9
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/copier v0.3.5
	github.com/leprosus/golang-clickhouse v1.0.0
	github.com/leprosus/golang-composer v1.0.0 // indirect
	github.com/lib/pq v1.10.2
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/mileusna/crontab v1.2.0
	github.com/prometheus/common v0.7.0
	github.com/siddontang/go v0.0.0-20180604090527-bdc77568d726 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gorm.io/driver/clickhouse v0.3.1
	gorm.io/driver/mysql v1.3.2
	gorm.io/driver/postgres v1.3.1
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.1
)
