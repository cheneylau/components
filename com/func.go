package com

import "github.com/gin-gonic/gin"

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Add("Access-Control-Allow-Headers", "*")
		c.Writer.Header().Add("Access-Control-Allow-Methods", "*")
		c.Next()
	}
}
