package main

import (
	"gitee.com/cheneylau/components/task"
	"gitee.com/cheneylau/components/web-server"
)

func main() {
	go task.StartTasks()
	go web_server.StartHttpServer()
	select {}
}

